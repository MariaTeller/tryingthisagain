name = "General"
last_name = "Kenobi"
print("Hello there, %s %s" % (name, last_name))
print("Hello there, %(name)s %(last_name)s" % {"name": name, "last_name": last_name})
print("Hello there, {} {}".format(name, last_name))
print("Hello there, {name} {last_name}".format(name=name, last_name=last_name))
print(f"Hello there, {name} {last_name}")

a=2
b=7
print(f"{a} times {b} raised to power of 2 is {(a * b) **2}.")
header1 = "Name"
header2 = "Age"
name = "John"
age = 22

print(f"| {header1:10} | {header2:10} |")
print("-" * 27)
print(f"| {name:10} | {age:10} |")

#Changing how variable is displayed
n = 109.4321888811111
print(f"{n:.3f}")

voters_percentage = 0.71
print(f"{voters_percentage:.1%}")

header1 = "Name"
header2 = "Age"
header3 = "Salary"
name = "John Doe"
name1="John Wick"
name2="Jeff Bezos"
age = 27
age1=40
age2=45
salary = "123456.00"
salary1="50000.00"
salary2="999999999.95"
print(f"| {header1:15} | {header2:5} | {header3:12}")
print("-" * 40)
print(f"| {name:15} | {age:5} | {salary:12}")
print(f"| {name1:15} | {age1:5} | {salary1:12}")
print(f"| {name2:15} | {age2:5} | {salary2:12}")